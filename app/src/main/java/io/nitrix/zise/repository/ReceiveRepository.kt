package io.nitrix.zise.repository

import android.graphics.Bitmap
import bitcoinunlimited.libbitcoincash.PayDestination
import bitcoinunlimited.libbitcoincash.PlatformContext
import bitcoinunlimited.libbitcoincash.Wallet
import io.nitrix.zise.utils.textToQREncode

class ReceiveRepository(
    private val context: PlatformContext,
) {

    private var destination: PayDestination? = null
    private var addressQR: Bitmap? = null

    suspend fun getAddressWithQRCode(wallet: Wallet, size: Int): Pair<String, Bitmap?> {
        val address = destination?.address
        return if (destination == null || address == null || addressQR == null || wallet.getBalanceIn(address) > 0) {
            refreshAddress(wallet, size)
        } else {
            Pair(destination?.address.toString(), addressQR)
        }
    }

    suspend fun refreshAddress(wallet: Wallet,size: Int): Pair<String, Bitmap?> {
        val newDestination = wallet.newDestination()
        val qrCode = textToQREncode(context, newDestination.address.toString(), size)
        destination = newDestination
        addressQR = qrCode
        return Pair(destination?.address.toString(), addressQR)
    }

    fun getResultAddressWithQRCode(amount: Double, size: Int): Pair<String, Bitmap?> {
        val resultAddress = destination?.address.toString() + "?amount=" + amount.toString()
        val resultQRCode = textToQREncode(context, resultAddress, size)
        return Pair(resultAddress, resultQRCode)
    }
}