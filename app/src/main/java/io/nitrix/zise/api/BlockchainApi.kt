package io.nitrix.zise.api

import com.haroldadmin.cnradapter.NetworkResponse
import io.nitrix.zise.data.response.BlockchainInfoResponse
import io.nitrix.zise.data.response.HeaderInfo
import io.nitrix.zise.data.response.HeaderInfoResponse
import io.nitrix.zise.data.response.NetworkError
import retrofit2.http.GET
import retrofit2.http.Path

interface BlockchainApi {

    @GET("api/utxo-summary/")
    suspend fun getBlockchainInfo(): NetworkResponse<BlockchainInfoResponse, NetworkError>

    @GET("api/blocks-by-height/{height}")
    suspend fun getHeaderByHeight(
        @Path("height") height: Long
    ): NetworkResponse<HeaderInfoResponse, NetworkError>

}