package io.nitrix.zise.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import bitcoinunlimited.libbitcoincash.TransactionHistory
import io.nitrix.zise.data.model.TransactionNote
import io.nitrix.zise.repository.WalletRepository
import io.nitrix.zise.utils.fromSatoshiAmount
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

data class TransactionDetailsState(
    val amount: Double,
    val exchangeAmount: Double,
    val sendUri: String?,
    val receiveUri: String?,
    val fee: Double,
    val exchangeFee: Double,
    val info: String?,
)

class TransactionDetailsViewModel(
    private val walletRepository: WalletRepository
) : ViewModel() {

    private var currentTransactionId: String? = null

    private val _transactionDetailsStateFlow = MutableStateFlow(
        TransactionDetailsState(
            amount = 0.0,
            exchangeAmount = 0.0,
            sendUri = null,
            receiveUri = null,
            fee = 0.0,
            exchangeFee = 0.0,
            info = null,
        )
    )

    val transactionDetailsStateFlow = _transactionDetailsStateFlow.asStateFlow()

    fun fetchTransaction(transactionId: String) = viewModelScope.launch(Dispatchers.IO) {
        currentTransactionId = transactionId
        walletRepository.getTransactionHistory(transactionId)?.let { transactionHistory ->
            val amountAndExchange = getAmountAndExchange(transactionHistory)
            val feeAndExchange = getFeeAndExchange(transactionHistory)
            val receiveAndSendUri = getReceiveAndSendUri(transactionHistory)
            val info = getInfoForTransaction(transactionHistory)

            _transactionDetailsStateFlow.update { transaction ->
                transaction.copy(
                    amount = amountAndExchange.first,
                    exchangeAmount = amountAndExchange.second,
                    sendUri = receiveAndSendUri.first,
                    receiveUri = receiveAndSendUri.second,
                    fee = feeAndExchange.first,
                    exchangeFee = feeAndExchange.second,
                    info = info
                )
            }
        }
    }

    private fun getAmountAndExchange(transaction: TransactionHistory): Pair<Double, Double> {
        val amount = fromSatoshiAmount(transaction.incomingAmt - transaction.outgoingAmt)
        val exchange = amount * walletRepository.exchange
        return Pair(amount, exchange)
    }

    private fun getFeeAndExchange(transaction: TransactionHistory): Pair<Double, Double> {
        val amount = fromSatoshiAmount(transaction.tx.fee)
        val exchange = amount * walletRepository.exchange
        return Pair(amount, exchange)
    }

    private fun getInfoForTransaction(transaction: TransactionHistory) : String {
        walletRepository.getNoteForTransaction(transaction.tx.id.toHex())
            .onSuccess { note ->
                return note.ifEmpty { transaction.note }
            }.onFailure {
                return transaction.note
            }
        return EMPTY_NOTE
    }

    fun saveInfoForTransaction(note: String) = viewModelScope.launch(Dispatchers.IO) {
        currentTransactionId?.let {
            walletRepository.saveNoteForTransaction(TransactionNote(it, note))
        }
    }

    private fun getReceiveAndSendUri(transaction: TransactionHistory): Pair<String?, String?> {

        val outputs = transaction.tx.outputs.map { it.script.address.toString() }.filter { it.isNotEmpty() }

        return Pair(outputs.getOrNull(0), outputs.getOrNull(1))
    }

    companion object {
        private const val EMPTY_NOTE = ""
    }

}