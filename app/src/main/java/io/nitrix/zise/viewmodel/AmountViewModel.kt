package io.nitrix.zise.viewmodel

import androidx.lifecycle.ViewModel
import io.nitrix.zise.repository.WalletRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlin.math.pow

data class AmountScreenState(
        val inputString: List<String>,
        val predefinedAmount: Pair<Double, Double>?,
        val isSwapped: Boolean
)

class AmountViewModel(
        private val walletRepository: WalletRepository
) : ViewModel() {

    private val _inputFlow = MutableStateFlow(AmountScreenState(listOf(), null, false))
    val inputFlow = _inputFlow.asStateFlow()

    fun changeInput(inputString: List<String>) {
        _inputFlow.update { it.copy(inputString = inputString) }
    }

    fun getInput(): List<String> {
        return inputFlow.value.inputString
    }

    fun setPredefinedAmount(satoshiAmount: String?) {
        val amount = satoshiAmount?.toDouble() ?: 0.0
        val exchangeAmount = amount * walletRepository.exchange
        _inputFlow.update { it.copy(predefinedAmount = Pair(amount, exchangeAmount)) }
    }

    fun convertAmountToNexa(inputString: List<String>): Double {
        if (walletRepository.exchange == 0.0) return 0.0
        return parseInput(inputString) / walletRepository.exchange
    }

    fun convertAmountToCurrency(inputString: List<String>): Double {
        return parseInput(inputString) * walletRepository.exchange
    }

    fun swapInput() {
        _inputFlow.update { it.copy(inputString = listOf(), isSwapped = !it.isSwapped) }
    }

    fun parseInput(inputString: List<String>): Double {
        var amount = 0.0
        val pointIndex = inputString.indexOf(".")
        if (pointIndex == -1) {
            inputString.forEachIndexed { index, symbol ->
                amount += (symbol.toIntOrNull() ?: 0) * 10.0.pow(inputString.lastIndex - index)
            }
        } else {
            val integers = inputString.subList(0, pointIndex)
            val fractions = inputString.subList(pointIndex + 1, inputString.size)
            integers.forEachIndexed { index, symbol ->
                amount += (symbol.toIntOrNull() ?: 0) * 10.0.pow(integers.lastIndex - index)
            }
            fractions.forEachIndexed { index, symbol ->
                amount += (symbol.toIntOrNull() ?: 0) * 10.0.pow(-(index + 1))
            }
        }
        return amount
    }
}