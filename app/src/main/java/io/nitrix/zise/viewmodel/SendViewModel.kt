package io.nitrix.zise.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import bitcoinunlimited.libbitcoincash.*
import io.nitrix.zise.data.model.TransactionNote
import io.nitrix.zise.data.response.NetworkError
import io.nitrix.zise.repository.PinCodeRepository
import io.nitrix.zise.repository.WalletRepository
import io.nitrix.zise.utils.fromSatoshiAmount
import io.nitrix.zise.utils.toSatoshiAmount
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

data class SendScreenState(
    val isLoading: Boolean,
    val url: String?,
    val amount: Double,
    val usdAmount: Double,
    val isExchangeUpdated: Boolean,
    val error: NetworkError?,
)

class SendViewModel(
    private val walletRepository: WalletRepository,
    private val pinCodeRepository: PinCodeRepository
) : ViewModel() {

    private var transaction: iTransaction? = null

    private val _sendScreenFlow = MutableStateFlow(
        SendScreenState(
            true,
            null,
            0.0,
            0.0,
            false,
            null
        )
    )
    val sendScreenFlow = _sendScreenFlow.asStateFlow()

    val isPinCodeSet: Boolean
        get() = pinCodeRepository.pinCode.isNullOrEmpty().not()

    fun setSendingUrl(url: String) {
        _sendScreenFlow.tryEmit(
            SendScreenState(
                false,
                url,
                0.0,
                0.0,
                false,
                null
            )
        )
    }

    fun setAmount(amount: Double) {
        _sendScreenFlow.tryEmit(
            SendScreenState(
                false,
                _sendScreenFlow.value.url,
                amount,
                amount * walletRepository.exchange ,
                false,
                null
            )
        )
    }

    fun setFiatAmount(amount: Double) {
        _sendScreenFlow.tryEmit(
                SendScreenState(
                        false,
                        _sendScreenFlow.value.url,
                        amount / walletRepository.exchange ,
                        amount,
                        false,
                        null
                )
        )
    }

    fun send(note: String, callback: (Boolean) -> Unit, errorCallback: (String) -> Unit) =
        viewModelScope.launch(Dispatchers.IO) {
            val amount = toSatoshiAmount(_sendScreenFlow.value.amount)
            val address = try {
                PayAddress(_sendScreenFlow.value.url?.split("?amount=")?.first()?.trim() ?: "")
            } catch (e: UnknownBlockchainException) {
                e.printStackTrace()
                withContext(Dispatchers.Main) {
                    errorCallback(INCORRECT_ADDRESS_ERROR)
                }
                return@launch
            }
            walletRepository.send(amount, address, note)
                .onSuccess {
                    transaction = it
                    withContext(Dispatchers.Main) {
                        callback(transaction != null)
                    }
                }
                .onFailure {
                    val error = if (it is WalletNotEnoughBalanceException) {
                        NOT_ENOUGH_BALANCE_ERROR
                    } else {
                        SENDING_TRANSACTION_ERROR
                    }
                    withContext(Dispatchers.Main) {
                        errorCallback(error)
                    }
                }
        }

    fun updateExchange() = viewModelScope.launch(Dispatchers.IO) {
        _sendScreenFlow.emit(
            SendScreenState(
                true,
                _sendScreenFlow.value.url,
                _sendScreenFlow.value.amount,
                _sendScreenFlow.value.usdAmount,
                _sendScreenFlow.value.isExchangeUpdated,
                null
            )
        )
        val result = walletRepository.updateExchange()
        _sendScreenFlow.emit(
            SendScreenState(
                true,
                _sendScreenFlow.value.url,
                _sendScreenFlow.value.amount,
                _sendScreenFlow.value.usdAmount,
                true,
                result.error
            )
        )
    }

    fun getSuggestedFee(): Pair<Double, Double> {
        walletRepository.prepareSend(toSatoshiAmount(sendScreenFlow.value.amount))?.let {
            val fee = fromSatoshiAmount(it.fee)
            val exchangeFee = fee * walletRepository.exchange
            walletRepository.abortTransaction(it)
            return Pair(fee, exchangeFee)
        }
        return Pair(0.0, 0.0)
    }

    fun getFee(): Pair<Double, Double> {
        return transaction?.let {
            val fee = fromSatoshiAmount(it.fee)
            val exchangeFee = fee * walletRepository.exchange
            walletRepository.abortTransaction(it)
            return Pair(fee, exchangeFee)
        } ?: Pair(0.0, 0.0)
    }

    fun getTransactionId(): String {
        return transaction?.id.toString()
    }

    fun saveInfoForTransaction(note: String) = viewModelScope.launch(Dispatchers.IO) {
        transaction?.let {
            walletRepository.saveNoteForTransaction(TransactionNote(it.id.toHex(), note))
        }
    }

    companion object {
        private const val NOT_ENOUGH_BALANCE_ERROR = "Not enough balance"
        private const val SENDING_TRANSACTION_ERROR = "Error when sending transaction"
        private const val INCORRECT_ADDRESS_ERROR = "Incorrect address"
    }
}