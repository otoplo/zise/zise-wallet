package io.nitrix.zise.viewmodel

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.pdf.PdfDocument
import android.os.Environment
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.nitrix.zise.R
import io.nitrix.zise.repository.PinCodeRepository
import io.nitrix.zise.repository.WalletRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream


class SettingsViewModel(
    private val walletRepository: WalletRepository,
    private val pinCodeRepository: PinCodeRepository
) : ViewModel() {

    val isPinCodeSet: Boolean
        get() = pinCodeRepository.pinCode.isNullOrEmpty().not()

    fun getSecretWords(): List<String> {
        return walletRepository.getWallet().secretWords.split(" ")
    }

    fun generatePDF(context: Context, text: String) = viewModelScope.launch(Dispatchers.IO) {

        val pageHeight = 1120
        val pageWidth = 792

        val pdfDocument = PdfDocument()

        val title = Paint()

        val myPageInfo: PdfDocument.PageInfo? =
            PdfDocument.PageInfo.Builder(pageWidth, pageHeight, 1).create()

        val myPage: PdfDocument.Page = pdfDocument.startPage(myPageInfo)

        val canvas: Canvas = myPage.canvas

        title.typeface = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL)

        title.textSize = 16F

        title.color = ContextCompat.getColor(context, R.color.black)

        title.textAlign = Paint.Align.CENTER

        canvas.drawText(text, 396F, 560F, title)

        pdfDocument.finishPage(myPage)

        val file = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
            FILE_NAME
        )

        val answer = if (file.exists()) {
            "$FILE_NAME is existed"
        } else {
            try {
                pdfDocument.writeTo(FileOutputStream(file))
                "$FILE_NAME generated to Downloads"
            } catch (e: Exception) {
                e.printStackTrace()
                "Fail to generate PDF file.."
            }
        }

        withContext(Dispatchers.Main) {
            Toast.makeText(context, answer, Toast.LENGTH_SHORT).show()
        }

        pdfDocument.close()
    }

    fun savePDF(context: Context) = viewModelScope.launch(Dispatchers.IO) {

        val path =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        val file = File(path, NEW_FILE)

        if (file.exists()) file.delete()

        val answer = if (file.exists()) {
            "$NEW_FILE is existed"
        } else {
            try {
                val fileInputStream = context.assets.open(NEW_FILE)
                val fileOutputStream = FileOutputStream(file)

                copyFile(fileInputStream, fileOutputStream)

                "$NEW_FILE save to Downloads"
            } catch (e: Exception) {
                e.printStackTrace()
                "Fail to save PDF file.."
            }
        }

        withContext(Dispatchers.Main) {
            Toast.makeText(context, answer, Toast.LENGTH_SHORT).show()
        }

    }

    private fun copyFile(fileInputStream: InputStream, fileOutputStream: OutputStream) {
        fileInputStream.use { fis ->
            fileOutputStream.use { os ->
                val buffer = ByteArray(1024)
                var len: Int
                while (fis.read(buffer).also { len = it } != -1) {
                    os.write(buffer, 0, len)
                }
            }
        }
    }

    companion object {

        private const val FILE_NAME = "SecretWords.pdf"
        private const val NEW_FILE = "Zise wallet backup.pdf"
    }
}