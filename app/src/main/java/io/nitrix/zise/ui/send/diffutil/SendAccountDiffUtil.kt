package io.nitrix.zise.ui.send.diffutil

import androidx.recyclerview.widget.DiffUtil
import io.nitrix.zise.data.model.AccountForTransaction

class SendAccountDiffUtil : DiffUtil.ItemCallback<AccountForTransaction>() {

    override fun areItemsTheSame(oldItem: AccountForTransaction, newItem: AccountForTransaction): Boolean {
        return oldItem.cryptoType == newItem.cryptoType
    }

    override fun areContentsTheSame(oldItem: AccountForTransaction, newItem: AccountForTransaction): Boolean {
        return oldItem.cryptoType == newItem.cryptoType &&
                oldItem.selected == newItem.selected
    }
}