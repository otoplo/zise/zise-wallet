package io.nitrix.zise.ui.receive

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import io.nitrix.zise.R
import io.nitrix.zise.data.enumes.CryptoType
import io.nitrix.zise.data.model.AccountForTransaction
import io.nitrix.zise.databinding.FragmentReceiveBinding
import io.nitrix.zise.ui.decorations.HorizontalListItemDecoration
import io.nitrix.zise.ui.send.adapter.SendAccountsAdapter
import io.nitrix.zise.utils.createBitmapUri
import io.nitrix.zise.utils.textCopyThenPost
import io.nitrix.zise.viewmodel.ReceiveScreenState
import io.nitrix.zise.viewmodel.ReceiveViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class ReceiveFragment : Fragment(R.layout.fragment_receive) {

    private val viewBinding: FragmentReceiveBinding by viewBinding(FragmentReceiveBinding::bind)

    private val receiveAccountsAdapter by lazy { SendAccountsAdapter() }

    private val receiveViewModel by activityViewModel<ReceiveViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                receiveViewModel.receiveScreenFlow.collectLatest { updateReceiveScreenUI(it) }
            }
        }
        receiveViewModel.fetchInitialAddressWithQRImage(
            minOf(
                viewBinding.qrCodeImage.layoutParams.width,
                viewBinding.qrCodeImage.layoutParams.height,
                1024
            ) + 200
        )

        context?.let { context ->
            with(viewBinding.sendAccountRecyclerView) {
                addItemDecoration(HorizontalListItemDecoration(resources.getDimensionPixelSize(R.dimen.margin_4)))
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = receiveAccountsAdapter
            }
            receiveAccountsAdapter.submitList(
                listOf(
                    AccountForTransaction(CryptoType.NEXA, true)
                )
            )

            viewBinding.refreshButton.setOnClickListener {
                receiveViewModel.refreshAddress(
                    minOf(
                        viewBinding.qrCodeImage.layoutParams.width,
                        viewBinding.qrCodeImage.layoutParams.height,
                        1024
                    ) + 200
                )
            }

            viewBinding.requestButton.setOnClickListener {
                receiveViewModel.updateExchange()

            }
            viewBinding.shareButton.setOnClickListener {
                val shareIntent = createShareIntent()
                context.startActivity(shareIntent)
            }

            viewBinding.receiveUriText.setOnLongClickListener {
                textCopyThenPost(requireContext(), receiveViewModel.receiveScreenFlow.value.address ?: "")
                true
            }
        }
    }

    private fun updateReceiveScreenUI(state: ReceiveScreenState) {
        if (!state.isLoading) {
            viewBinding.qrCodeImage.setImageBitmap(state.qrCode)
            viewBinding.receiveUriText.text = state.address
        }
        if (state.isExchangeUpdated) {
            findNavController().navigate(
                ReceiveFragmentDirections.actionReceiveFragmentToReceiveAmountFragment()
            )
        }
    }

    private fun createShareIntent(): Intent {
        val bitmapUri = context?.let { context ->
            receiveViewModel.receiveScreenFlow.value.qrCode?.let { createBitmapUri(context, it) }
        }
        return Intent.createChooser(Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, viewBinding.receiveUriText.text.toString())
            putExtra(Intent.EXTRA_STREAM, bitmapUri)
            type = "*/*"
        }, null)
    }
}