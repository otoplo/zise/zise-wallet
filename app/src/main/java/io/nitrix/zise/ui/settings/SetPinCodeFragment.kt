package io.nitrix.zise.ui.settings

import androidx.navigation.fragment.findNavController
import io.nitrix.zise.R
import io.nitrix.zise.ui.input.PinCodeFragment

class SetPinCodeFragment : PinCodeFragment() {

    override val titleId: Int by lazy {
        if (pinCodeViewModel.isPinCodeSet) R.string.settings_set_your_new_pin
        else R.string.settings_set_your_pin
    }

    override val resultCallback: (Boolean) -> Unit = {
        findNavController().navigate(
            SetPinCodeFragmentDirections.actionSetPinCodeFragmentToConfirmPinCodeFragment(
                pinCodeViewModel.getInput().joinToString(separator = "").trim()
            )
        )
    }

}