package io.nitrix.zise.ui.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import io.nitrix.zise.R
import io.nitrix.zise.data.model.AccountItem
import io.nitrix.zise.databinding.FragmentAccountsBinding
import io.nitrix.zise.ui.decorations.VerticalListItemDecoration
import io.nitrix.zise.ui.home.adapter.AccountsAdapter
import io.nitrix.zise.utils.toFiatFormat
import io.nitrix.zise.viewmodel.HomeViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class AccountsFragment : Fragment(R.layout.fragment_accounts) {

    private val viewBinding: FragmentAccountsBinding by viewBinding(FragmentAccountsBinding::bind)

    private val homeViewModel by activityViewModel<HomeViewModel>()

    private val accountsAdapter by lazy { AccountsAdapter(::onItemClick) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                homeViewModel.accountsFlow.collectLatest {
                    if (it == null) return@collectLatest
                    updateAccountsUI(it)
                }
            }
        }

        context?.let { context ->

            with(viewBinding.accountRecyclerView) {
                addItemDecoration(VerticalListItemDecoration(resources.getDimensionPixelSize(R.dimen.margin_4)))
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = accountsAdapter
            }
        }
    }

    override fun onStart() {
        super.onStart()
        homeViewModel.restart()
        homeViewModel.startSyncingWallet()
    }

    override fun onStop() {
        super.onStop()
        homeViewModel.stopSyncingWallet()
    }

    private fun updateAccountsUI(account: AccountItem.Account) {
        val accounts = listOf(account)
        val list = mutableListOf<AccountItem>().apply {
            addAll(accounts)
            add(AccountItem.Info)
        }
        accountsAdapter.submitList(list)
        updateBalance(accounts.sumOf { it.exchangeAmount })
    }

    private fun updateBalance(amount: Double) {
        viewBinding.balanceText.text = getString(R.string.value_in_usd, toFiatFormat(amount))
    }

    private fun onItemClick(item: AccountItem.Account) {
        val action = AccountsFragmentDirections
            .actionAccountsFragmentToTransactionsFragment(item)
        findNavController().navigate(action)
    }
}