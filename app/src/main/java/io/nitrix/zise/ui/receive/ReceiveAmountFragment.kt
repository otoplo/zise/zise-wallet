package io.nitrix.zise.ui.receive

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import io.nitrix.zise.ui.input.AmountFragment
import io.nitrix.zise.viewmodel.ReceiveViewModel
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class ReceiveAmountFragment : AmountFragment() {

    private val receiveViewModel by activityViewModel<ReceiveViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let { context ->
            viewBinding.confirmButton.setOnClickListener {
                val input = amountViewModel.inputFlow.value.inputString
                if (input.isEmpty() || input.all { it == "0" || it == "." }) {
                    Toast.makeText(context, "Amount should be more than 0", Toast.LENGTH_LONG).show()
                } else {
                    receiveViewModel.setFiatAmount(amountViewModel.parseInput(input))
                    findNavController().navigate(
                        ReceiveAmountFragmentDirections.actionReceiveAmountFragmentToReceiveResultFragment()
                    )
                }
            }
        }
    }
}