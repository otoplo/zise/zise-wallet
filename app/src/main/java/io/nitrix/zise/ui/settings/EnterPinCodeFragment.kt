package io.nitrix.zise.ui.settings

import android.graphics.drawable.Drawable
import android.graphics.drawable.TransitionDrawable
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import io.nitrix.zise.R
import io.nitrix.zise.ui.input.PinCodeFragment
import io.nitrix.zise.utils.NAVIGATION_FRAGMENT_RESULT
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class EnterPinCodeFragment : PinCodeFragment() {

    override val titleId: Int = R.string.enter_your_pin

    private val args: EnterPinCodeFragmentArgs by navArgs()

    private var isError = false

    override val resultCallback: (Boolean) -> Unit = { isSuccess ->

        if (isSuccess) {
            findNavController().apply {
                previousBackStackEntry?.savedStateHandle?.set(
                    NAVIGATION_FRAGMENT_RESULT,
                    args.inputType
                )
            }
            pinCodeViewModel.resetErrorInput()
            findNavController().popBackStack()
        } else {
            if (!isError) showErrorBackground()
            pinCodeViewModel.changeInput(listOf())
            pinCodeViewModel.setErrorInput()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                pinCodeViewModel.waitingTime.collectLatest { time ->
                    val visibility = time.isNullOrEmpty()

                    with(viewBinding) {
                        lockImage.isVisible = !visibility
                        waitingTimeTextView.isVisible = !visibility
                        pincodeLayout.isVisible = visibility
                        waitingTimeTextView.text = time
                    }

                    if (isError == !visibility) return@collectLatest

                    isError = !visibility
                    if (isError) {
                        onLock()
                    } else {
                        onUnlock()
                    }
                }
            }
        }

        pinCodeViewModel.startTimer()
    }

    private fun onLock() {
        val headerDrawables =
            arrayOf(
                ContextCompat.getDrawable(requireContext(), R.drawable.default_background),
                ContextCompat.getDrawable(requireContext(), R.drawable.error_background)
            )

        setAndStartTransaction(headerDrawables)
    }

    private fun onUnlock() {
        val headerDrawables =
            arrayOf(
                ContextCompat.getDrawable(requireContext(), R.drawable.error_background),
                ContextCompat.getDrawable(requireContext(), R.drawable.default_background)
            )

        setAndStartTransaction(headerDrawables)
    }

    private fun setAndStartTransaction(drawables: Array<Drawable?>) {
        val transactions = TransitionDrawable(drawables)

        viewBinding.headerLayout.background = transactions

        transactions.startTransition(ANIMATION_DURATION)
    }

    override fun onStart() {
        super.onStart()
        pinCodeViewModel.startTimer()
    }

    override fun onStop() {
        super.onStop()
        pinCodeViewModel.stopTimer()
    }

    companion object {
        private const val ANIMATION_DURATION = 1000
    }
}