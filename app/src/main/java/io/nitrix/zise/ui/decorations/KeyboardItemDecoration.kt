package io.nitrix.zise.ui.decorations

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class KeyboardItemDecoration(
    private val verticalSpace: Int,
    private val horizontalSpace: Int,
    private val spanCount: Int,
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State,
    ) {
        val position = parent.getChildAdapterPosition(view)
        val childrenCount = parent.adapter?.itemCount ?: 0
        val itemsOnBottomRow = childrenCount % spanCount
        with(outRect) {
            top = if (position < spanCount) 0 else verticalSpace / 2
            left = horizontalSpace / 2
            right = horizontalSpace / 2
            bottom = if (childrenCount - position <= if (itemsOnBottomRow == 0) spanCount else itemsOnBottomRow) 0 else verticalSpace / 2
        }
    }
}