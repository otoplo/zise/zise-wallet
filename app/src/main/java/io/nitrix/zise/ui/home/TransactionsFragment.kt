package io.nitrix.zise.ui.home

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import io.nitrix.zise.R
import io.nitrix.zise.data.enumes.SortType
import io.nitrix.zise.data.model.Transaction
import io.nitrix.zise.databinding.FragmentTransactionsBinding
import io.nitrix.zise.ui.decorations.VerticalListItemDecoration
import io.nitrix.zise.ui.home.adapter.TransactionsAdapter
import io.nitrix.zise.viewmodel.TransactionViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class TransactionsFragment : Fragment(R.layout.fragment_transactions) {

    private val viewBinding: FragmentTransactionsBinding by viewBinding(FragmentTransactionsBinding::bind)

    private val transactionViewModel by activityViewModel<TransactionViewModel>()

    private val transactionsAdapter by lazy { TransactionsAdapter(::onItemClick) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                transactionViewModel.transactionsFlow.collectLatest { updateTransactionsList(it) }
            }
        }
        context?.let { context ->
            setSortType(transactionViewModel.sortType)

            viewBinding.sortDownButton.setOnClickListener { setSortType(SortType.RECEIVED) }
            viewBinding.sortBothButton.setOnClickListener { setSortType(SortType.BOTH) }
            viewBinding.sortUpButton.setOnClickListener { setSortType(SortType.SENT) }

            viewBinding.searchText.setOnEditorActionListener { textView, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    transactionViewModel.performSearch(textView.text.toString())
                    return@setOnEditorActionListener true
                }
                return@setOnEditorActionListener false
            }
            with(viewBinding.transactionRecyclerView) {
                addItemDecoration(VerticalListItemDecoration(resources.getDimensionPixelSize(R.dimen.margin_4)))
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = transactionsAdapter
            }
        }
    }

    override fun onStart() {
        super.onStart()
        transactionViewModel.fetchTransactions()
    }

    private fun updateTransactionsList(transactions: List<Transaction>) {
        transactionsAdapter.submitList(transactions)
    }

    private fun onItemClick(item: Transaction) {
        val action = TransactionsFragmentDirections
            .actionTransactionsFragmentToTransactionDetailsFragment(item.id)
        findNavController().navigate(action)
    }

    private fun setSortType(sortType: SortType) {
        transactionViewModel.setSort(sortType)
        when (sortType) {
            SortType.RECEIVED -> {
                viewBinding.sortDownButton.setImageResource(R.drawable.ic_sort_down_selected)
                viewBinding.sortBothButton.setImageResource(R.drawable.ic_sort_both)
                viewBinding.sortUpButton.setImageResource(R.drawable.ic_sort_up)
            }
            SortType.BOTH -> {
                viewBinding.sortDownButton.setImageResource(R.drawable.ic_sort_down)
                viewBinding.sortBothButton.setImageResource(R.drawable.ic_sort_both_selected)
                viewBinding.sortUpButton.setImageResource(R.drawable.ic_sort_up)
            }
            SortType.SENT -> {
                viewBinding.sortDownButton.setImageResource(R.drawable.ic_sort_down)
                viewBinding.sortBothButton.setImageResource(R.drawable.ic_sort_both)
                viewBinding.sortUpButton.setImageResource(R.drawable.ic_sort_up_selected)
            }
        }
    }
}