package io.nitrix.zise.ui.send

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import io.nitrix.zise.ui.input.AmountFragment
import io.nitrix.zise.viewmodel.SendViewModel
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class SendAmountFragment : AmountFragment() {

    private val sendViewModel by activityViewModel<SendViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val splittedUrl = sendViewModel.sendScreenFlow.value.url?.split("?amount=")
        val amount = if (splittedUrl?.size == 1) 0.0 else splittedUrl?.last()?.toDouble() ?: 0.0
        sendViewModel.setAmount(amount)
        if (splittedUrl?.size != 1) {
            amountViewModel.setPredefinedAmount(splittedUrl?.last())
        }

        viewBinding.confirmButton.setOnClickListener {
            val input = amountViewModel.inputFlow.value.inputString
            val predefinedAmount = amountViewModel.inputFlow.value.predefinedAmount
            if ((input.isEmpty() || input.all { it == "0" || it == "." }) && predefinedAmount == null) {
                Toast.makeText(context, "Amount should be more than 0", Toast.LENGTH_LONG).show()
            } else {
                if (predefinedAmount != null) {
                    sendViewModel.setAmount(amount)
                } else {
                    val fiatAmount =
                        amountViewModel.parseInput(amountViewModel.inputFlow.value.inputString)
                    if (amountViewModel.inputFlow.value.isSwapped) {
                        sendViewModel.setAmount(fiatAmount)
                    } else {
                        sendViewModel.setFiatAmount(fiatAmount)
                    }
                }
                findNavController().navigate(
                    SendAmountFragmentDirections.actionSendAmountFragmentToSendDetailsFragment(
                        amountViewModel.inputFlow.value.isSwapped
                    )
                )
            }
        }
    }
}