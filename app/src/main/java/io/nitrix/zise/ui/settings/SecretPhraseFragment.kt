package io.nitrix.zise.ui.settings


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import io.nitrix.zise.R
import io.nitrix.zise.databinding.FragmentSecretPhraseBinding
import io.nitrix.zise.ui.decorations.GridItemDecoration
import io.nitrix.zise.ui.settings.adapter.SecretWordAdapter
import io.nitrix.zise.viewmodel.SettingsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class SecretPhraseFragment : Fragment(R.layout.fragment_secret_phrase) {

    private val viewBinding: FragmentSecretPhraseBinding by viewBinding(FragmentSecretPhraseBinding::bind)

    private val secretWordAdapter by lazy { SecretWordAdapter() }

    private val settingsViewModel by viewModel<SettingsViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let { context ->
            with(viewBinding.secretWordsRecyclerView) {
                addItemDecoration(
                    GridItemDecoration(
                        resources.getDimensionPixelSize(R.dimen.margin_4),
                        GRID_SIZE
                    )
                )
                layoutManager = FlexboxLayoutManager(context).apply {
                    justifyContent = JustifyContent.CENTER
                    flexDirection = FlexDirection.ROW
                    flexWrap = FlexWrap.WRAP
                }
                adapter = secretWordAdapter
            }
            secretWordAdapter.submitList(settingsViewModel.getSecretWords())
        }
    }

    companion object {
        private const val GRID_SIZE = 3
    }
}