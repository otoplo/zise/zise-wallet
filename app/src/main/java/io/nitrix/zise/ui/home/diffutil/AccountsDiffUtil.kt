package io.nitrix.zise.ui.home.diffutil

import androidx.recyclerview.widget.DiffUtil
import io.nitrix.zise.data.model.AccountItem

class AccountsDiffUtil : DiffUtil.ItemCallback<AccountItem>() {

    override fun areItemsTheSame(oldItem: AccountItem, newItem: AccountItem): Boolean {
        return if (oldItem is AccountItem.Account && newItem is AccountItem.Account) {
            return oldItem.name == newItem.name
        } else {
            oldItem == newItem
        }
    }

    override fun areContentsTheSame(oldItem: AccountItem, newItem: AccountItem): Boolean {
        return if (oldItem is AccountItem.Account && newItem is AccountItem.Account) {
            return oldItem.name == newItem.name &&
                    oldItem.amount == newItem.amount &&
                    oldItem.isSynced == newItem.isSynced
        } else {
            oldItem == newItem
        }
    }
}