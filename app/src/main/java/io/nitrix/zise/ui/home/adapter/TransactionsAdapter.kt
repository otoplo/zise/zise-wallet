package io.nitrix.zise.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import io.nitrix.zise.data.model.Transaction
import io.nitrix.zise.ui.home.diffutil.TransactionDiffUtil
import io.nitrix.zise.ui.home.viewholder.TransactionViewHolder

class TransactionsAdapter(
    private val onClick: (Transaction) -> Unit = {}
) : ListAdapter<Transaction, TransactionViewHolder>(TransactionDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        TransactionViewHolder(LayoutInflater.from(parent.context), parent)

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.update(getItem(position), onClick)
    }
}