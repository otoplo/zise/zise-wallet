package io.nitrix.zise.ui.home.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isInvisible
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import by.kirich1409.viewbindingdelegate.viewBinding
import io.nitrix.zise.R
import io.nitrix.zise.data.model.AccountItem
import io.nitrix.zise.databinding.AccountItemLayoutBinding
import io.nitrix.zise.utils.toCryptoFormat
import io.nitrix.zise.utils.toFiatFormat

class AccountViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup
) : ViewHolder(inflater.inflate(R.layout.account_item_layout, parent, false)) {

    private val viewBinding: AccountItemLayoutBinding by viewBinding(AccountItemLayoutBinding::bind)

    fun update(
        item: AccountItem.Account,
        onClick: (AccountItem.Account) -> Unit = {}
    ) {
        itemView.setOnClickListener { onClick(item) }
        viewBinding.accountTitleText.text = item.name
        viewBinding.accountAmountText.text =
            itemView.context.getString(R.string.value_in_nex, toCryptoFormat(item.amount))
        viewBinding.accountExchangeText.text =
            itemView.context.getString(R.string.value_in_usd, toFiatFormat(item.exchangeAmount))
        viewBinding.syncingProgressBar.isInvisible = item.isSynced
    }
}