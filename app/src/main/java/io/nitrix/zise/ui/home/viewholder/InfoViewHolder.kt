package io.nitrix.zise.ui.home.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import io.nitrix.zise.R


class InfoViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup
) : ViewHolder(inflater.inflate(R.layout.account_item_info_layout, parent, false))