package io.nitrix.zise.ui.settings

import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.TransitionDrawable
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import io.nitrix.zise.R
import io.nitrix.zise.ui.input.PinCodeFragment
import kotlinx.coroutines.delay

class ConfirmPinCodeFragment : PinCodeFragment() {

    override val titleId: Int by lazy {
        if (pinCodeViewModel.isPinCodeSet) R.string.settings_confirm_your_new_pin
        else R.string.settings_confirm_your_pin
    }

    private val args: ConfirmPinCodeFragmentArgs by navArgs()

    override val resultCallback: (Boolean) -> Unit = {
        val pinCodeSet = args.pincodeSet.toCharArray().map { it.toString() }

        if (pinCodeViewModel.checkInput(pinCodeSet)) {
            pinCodeViewModel.setPinCode()
            findNavController().navigate(R.id.settings_nav_graph)
        } else {
            pinCodeViewModel.changeInput(listOf())
            showErrorBackground()
        }
    }
}