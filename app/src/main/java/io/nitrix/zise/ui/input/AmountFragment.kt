package io.nitrix.zise.ui.input

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import by.kirich1409.viewbindingdelegate.viewBinding
import io.nitrix.zise.R
import io.nitrix.zise.data.enumes.InputSymbol
import io.nitrix.zise.databinding.FragmentAmountBinding
import io.nitrix.zise.ui.input.adapter.InputAdapter
import io.nitrix.zise.ui.decorations.KeyboardItemDecoration
import io.nitrix.zise.utils.InputGridLayoutManager
import io.nitrix.zise.utils.toCryptoFormat
import io.nitrix.zise.utils.toFiatFormat
import io.nitrix.zise.viewmodel.AmountViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

abstract class AmountFragment : Fragment(R.layout.fragment_amount) {

    protected val viewBinding: FragmentAmountBinding by viewBinding(FragmentAmountBinding::bind)

    protected val amountViewModel by viewModel<AmountViewModel>()

    private val inputAdapter by lazy { InputAdapter(::onItemClick) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                amountViewModel.inputFlow.collectLatest {
                    if (it.predefinedAmount != null) {
                        updatePredefined(it.predefinedAmount)
                        return@collectLatest
                    }

                    if (it.isSwapped) {
                        updateAmount(it.inputString)
                    } else {
                        updateExchangeAmount(it.inputString)
                    }
                }
            }
        }

        context?.let { context ->

            viewBinding.swapButton.setOnClickListener {
                amountViewModel.swapInput()
            }

            with(viewBinding.inputRecyclerView) {
                addItemDecoration(
                    KeyboardItemDecoration(
                        resources.getDimensionPixelSize(R.dimen.margin_12),
                        resources.getDimensionPixelSize(R.dimen.margin_32),
                        GRID_SIZE
                    )
                )
                layoutManager = InputGridLayoutManager(context, GRID_SIZE).apply {
                    maxHeight = resources.getDimensionPixelSize(R.dimen.input_amount_height)
                }
                adapter = inputAdapter
            }
            inputAdapter.submitList(INPUT_SYMBOLS_LIST)
        }
    }

    private fun updatePredefined(amount: Pair<Double, Double>) {
        viewBinding.inputAmountText.text = getString(
            R.string.value_in_nex,
            toCryptoFormat(amount.first)
        )
        viewBinding.inputExchangeText.text = getString(
            R.string.value_in_usd,
            toFiatFormat(amount.second)
        )
    }

    private fun updateExchangeAmount(inputString: List<String>) {
        val exchangeAmount =
            if (inputString.isNotEmpty()) inputString.joinToString(separator = "") else "0"
        val amount = amountViewModel.convertAmountToNexa(inputString)
        viewBinding.inputAmountText.text = getString(
            R.string.value_in_nex,
            toCryptoFormat(amount)
        )
        viewBinding.inputExchangeText.text = getString(
            R.string.value_in_usd,
            exchangeAmount
        )
    }

    private fun updateAmount(inputString: List<String>) {
        val amount = if (inputString.isNotEmpty()) inputString.joinToString(separator = "") else "0"
        val exchangeAmount = amountViewModel.convertAmountToCurrency(inputString)
        viewBinding.inputAmountText.text = getString(
            R.string.value_in_usd,
            toFiatFormat(exchangeAmount)
        )
        viewBinding.inputExchangeText.text = getString(
            R.string.value_in_nex,
            amount
        )
    }

    private fun onItemClick(item: InputSymbol) {
        val currentInput = amountViewModel.getInput().toMutableList()
        when (item) {
            InputSymbol.ZERO -> {
                if (currentInput.size == 0) return
                if (!(currentInput.size == 1 && currentInput.contains(item.value))) {
                    currentInput.add(item.value)
                    amountViewModel.changeInput(currentInput)
                }
            }
            InputSymbol.POINT -> {
                if (!currentInput.contains(item.value)) {
                    if (currentInput.isEmpty()) {
                        currentInput.add(InputSymbol.ZERO.value)
                    }
                    currentInput.add(item.value)
                    amountViewModel.changeInput(currentInput)
                }
            }
            InputSymbol.BACK -> {
                if (amountViewModel.inputFlow.value.predefinedAmount != null) {
                    currentInput.add(InputSymbol.ZERO.value)
                    amountViewModel.changeInput(currentInput)
                }
                if (currentInput.isNotEmpty()) {
                    currentInput.removeAt(currentInput.lastIndex)
                    amountViewModel.changeInput(currentInput)
                }
            }
            else -> {
                currentInput.add(item.value)
                amountViewModel.changeInput(currentInput)
            }
        }
    }

    companion object {

        private const val GRID_SIZE = 3

        private val INPUT_SYMBOLS_LIST = listOf(
            InputSymbol.ONE,
            InputSymbol.TWO,
            InputSymbol.THREE,
            InputSymbol.FOUR,
            InputSymbol.FIVE,
            InputSymbol.SIX,
            InputSymbol.SEVEN,
            InputSymbol.EIGHT,
            InputSymbol.NINE,
            InputSymbol.POINT,
            InputSymbol.ZERO,
            InputSymbol.BACK,
        )
    }
}