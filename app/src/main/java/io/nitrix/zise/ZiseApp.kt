package io.nitrix.zise

import android.app.Application
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Initialize
import io.nitrix.zise.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class ZiseApp : Application() {

    override fun onCreate() {
        super.onCreate()

        System.loadLibrary("nexandroid")
        Initialize.LibBitcoinCash(ChainSelector.NEXATESTNET.v)

        startKoin {

            androidContext(this@ZiseApp)

            modules(listOf(
                viewModelModule,
                repositoryModule,
                networkModule,
                dbModule,
                utilsModule
            ))
        }
    }
}