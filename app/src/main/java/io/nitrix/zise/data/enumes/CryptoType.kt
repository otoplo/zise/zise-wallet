package io.nitrix.zise.data.enumes

enum class CryptoType { NEXA, BCH, XUSD, XBTC, XETH }