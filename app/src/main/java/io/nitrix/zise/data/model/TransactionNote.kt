package io.nitrix.zise.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "transaction_notes")
data class TransactionNote(
    @PrimaryKey
    val id: String,
    val note: String?
)
