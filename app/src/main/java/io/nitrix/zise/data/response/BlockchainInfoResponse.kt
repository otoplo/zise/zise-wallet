package io.nitrix.zise.data.response

import com.google.gson.annotations.SerializedName

data class BlockchainInfoResponse(
    @SerializedName("bestblock")
    val bestblock: String,
    @SerializedName("disk_size")
    val diskSize: Long,
    @SerializedName("hash_serialized")
    val hashSerialized: String,
    @SerializedName("height")
    val height: Long,
    @SerializedName("lastUpdated")
    val lastUpdated: Long,
    @SerializedName("total_amount")
    val totalAmount: Long,
    @SerializedName("txouts")
    val txouts: Long
)