package io.nitrix.zise.data.model

import io.nitrix.zise.data.enumes.CryptoType

data class AccountForTransaction(
    val cryptoType: CryptoType,
    var selected: Boolean
)
