package io.nitrix.zise.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import io.nitrix.zise.data.model.TransactionNote

@Dao
interface TransactionNoteDao {

    @Insert(onConflict = REPLACE)
    fun insert(transaction: TransactionNote)

    @Query("SELECT * FROM transaction_notes WHERE id = :transactionId")
    fun get(transactionId: String) : TransactionNote?

}