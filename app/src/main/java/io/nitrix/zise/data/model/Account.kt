package io.nitrix.zise.data.model

sealed interface AccountItem {
    data class Account(
        val name: String,
        val amount: Double,
        val exchangeAmount: Double,
        val isSynced: Boolean = false
    ) : java.io.Serializable, AccountItem

    object Info : AccountItem
}
